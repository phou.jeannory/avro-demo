package com.example.service;

import com.example.mapper.UserMapper;
import com.example.pojo.User;
import org.apache.avro.Schema;
import org.apache.avro.reflect.ReflectData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.env.Environment;
import org.springframework.core.env.StandardEnvironment;

import java.io.File;
import java.io.IOException;

public class AvroServiceImplTest {

    private UserMapper userMapper;

    private AvroServiceImpl avroService;

    private Environment environment;

    private String PRE_PATH;

    @Before
    public void setUp() throws Exception {
        userMapper = new UserMapper();
        avroService = new AvroServiceImpl();
        environment = new StandardEnvironment();
        PRE_PATH = environment.getProperty("application.prePath","/home/jeannory-e580/Projects/avro-demo/data/");
//        avroDemoApplication = new AvroDemoApplication();
//        userMapper = new UserMapper();
//        avroService = new AvroServiceImpl();
    }

    /**
     * from User[]
     * serialize to /data/out/users0.avro
     */
    @Test
    public void testSerializeUsersToAvroFromTab() throws IOException {
        final File file = new File(PRE_PATH + "out/" + "users0.avro");
        file.delete();
        final User[] users = new User[3];
        users[0] = new User(1, "john");
        users[1] = new User(2, "ken");
        users[2] = new User(3, "adam");
        avroService.serializeObjectsToAvro(users, "users0.avro");
    }

    /**
     * from /data/in/users1.json
     * serialize to /data/out/users1.avro
     */
    @Test
    public void testSerializeUsersToAvroFromJson() throws IOException {
        final File file = new File(PRE_PATH + "out/" + "users1.avro");
        file.delete();
        avroService.serializeObjectsToAvro(userMapper.jsonToUsers("users1.json"), "users1.avro");
    }

    /**
     * from /data/in/users1.json
     * serialize with schema /data/schema/UserSchema.json /data/out/users1-with-UserSchema.avro
     */
    @Test
    public void testSerializeUsersToAvroFromJsonWithSchema() throws IOException {
        final File file = new File(PRE_PATH + "out/" + "users1-with-UserSchema.avro");
        file.delete();
        avroService.serializeObjectsToAvro(userMapper.jsonToUsers("users1.json"), "users1-with-UserSchema.avro", "UserSchema.json");
     }

    /**
     * from /data/out/users1.avro
     * deserialize to User[]
     */
    @Test
    public void testDserializeAvroToUsers() throws IOException {
        final User[] results = avroService.deserializeAvroToUsers("users1.avro");
        Assert.assertEquals(3, results.length);
        Assert.assertEquals("Martin Riggs", results[0].getName());
    }

    /**
     * from /data/out/users1.avro
     * deserialize with schema /data/schema/UserSchema.json to User[]
     */
    @Test
    public void testDeserializeAvroToUsersWithSchema()throws IOException {
        final User[] results = avroService.deserializeAvroToUsers("users1.avro","UserSchema.json");
        Assert.assertEquals(3, results.length);
        Assert.assertEquals("Martin Riggs", results[0].getName());
    }

    @Test
    public void testDeserializeSchema() throws IOException {
        final Schema schema = avroService.deserializeSchema("UserSchemaBis.json");
        Assert.assertEquals("record",schema.getType().getName());
    }

    /**
     * from /data/schema/UserSchema.json
     * serialize to /data/schema/UserSchemaBis.json
     */
    @Test
    public void testWriteJsonSchema() throws IOException {
        final File file = new File(PRE_PATH + "schema/" + "UserSchemaBis.json");
        file.delete();
        final Schema schema = avroService.deserializeSchema("UserSchema.json");
        avroService.writeJsonSchema(schema, "UserSchemaBis.json");
    }

    /**
     * link to preview test
     * from /data/in/users1.json
     * serialize with schema /data/schema/UserSchemaBis.json to /data/out/users1-with-UserSchemaBis.avro
     */
    @Test
    public void testSerializeUsersToAvroFromJsonWithSchemaBis() throws IOException {
        final File file = new File(PRE_PATH + "out/" + "users1-with-UserSchemaBis.avro");
        file.delete();
        avroService.serializeObjectsToAvro(userMapper.jsonToUsers("users1.json"), "users1-with-UserSchemaBis.avro", "UserSchemaBis.json");
    }

    /**
     * from /data/schema/UserSchemaBis.json
     * serialize to /data/schema/UserSchemaBisBis.json
     */
    @Test
    public void testWriteJsonSchemaFromAvroV2() throws IOException {
        final File file = new File(PRE_PATH + "schema/" + "UserSchemaBisBis.json");
        file.delete();
        final Schema schema = avroService.deserializeSchema("UserSchemaBis.json");
        avroService.writeJsonSchema(schema, "UserSchemaBisBis.json");
    }

    /**
     * link to preview test
     * from /data/in/users1.json
     * serialize with schema /data/schema/UserSchemaBisBis.json to /data/out/users1-with-UserSchemaBisBis.avro
     */
    @Test
    public void testSerializeUsersToAvroFromJsonWithSchemaBisBis() throws IOException {
        final File file = new File(PRE_PATH + "out/" + "users1-with-UserSchemaBisBis.avro");
        file.delete();
        avroService.serializeObjectsToAvro(userMapper.jsonToUsers("users1.json"), "users1-with-UserSchemaBisBis.avro", "UserSchemaBisBis.json");
    }

    /**
     * test retrocompativy phase 1
     * build users2.avro from Object com.example.pojo.v2.User[]
     * serialize into /data/out/users2.avro
     */
    @Test
    public void testSerializeUsersToAvroFromNewObjectUser() throws IOException {
        final com.example.pojo.v2.User[] users = new com.example.pojo.v2.User[2];
        users[0] = new com.example.pojo.v2.User(1, "john", 30);
        users[1] = new com.example.pojo.v2.User(2, "sam", 18);
        avroService.serializeObjectsToAvroV2(users, "users2.avro");
    }

    /**
     * test retrocompativy phase 2
     * build Object com.example.pojo.v2.User[] Schema
     * deserialize from /data/out/users2.avro to  com.example.pojo.v2.User[] with /data/schema/users2Schema.json
     */
    @Test
    public void testWriteJsonSchemaFromNewObjectUser() throws IOException {
        final Schema schema = ReflectData.get().getSchema(com.example.pojo.v2.User.class);
        avroService.writeJsonSchema(schema, "users2Schema.json");
        final com.example.pojo.v2.User[] results = avroService.deserializeAvroToUsersV2("users2.avro", "users2Schema.json");
        Assert.assertEquals(2, results.length);
        Assert.assertEquals("john", results[0].getName());
        Assert.assertEquals(30, results[0].getAge());
    }

    /**
     * test retrocompativy phase 3
     * build Object com.example.pojo.v2.User[] Schema
     * deserialize from /data/out/users1.avro to  User[] with /data/schema/users2Schema.json
     */
    @Test
    public void testWriteJsonSchemaFromNewObjectUserBis() throws IOException {
        final User[] results = avroService.deserializeAvroToUsers("users1.avro", "users2Schema.json");
        Assert.assertEquals(3, results.length);
        Assert.assertEquals("Martin Riggs", results[0].getName());
    }

}