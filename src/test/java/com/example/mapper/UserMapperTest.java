package com.example.mapper;

import com.example.pojo.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.env.Environment;
import org.springframework.core.env.StandardEnvironment;

import java.io.IOException;

public class UserMapperTest {

    private UserMapper userMapper;

    private Environment environment;

    private String PRE_PATH;

    @Before
    public void setUp() throws Exception {
        userMapper = new UserMapper();
        environment = new StandardEnvironment();
        PRE_PATH = environment.getProperty("application.prePath","/home/jeannory-e580/Projects/avro-demo/data/");
    }

    /**
     * from /data/in/user1.json
     * deserialize to user
     */
    @Test
    public void testJsonToUser() throws IOException {
        final User result = userMapper.jsonToUser("user1.json");
        Assert.assertEquals("Martin Riggs", result.getName());
    }

    /**
     * from /data/in/users1.json
     * deserialize to users
     */
    @Test
    public void testJsonToUserList() throws IOException {
        final User[] results = userMapper.jsonToUsers("users1.json");
        final User user= results[0];
        Assert.assertEquals(3, results.length);
    }

}