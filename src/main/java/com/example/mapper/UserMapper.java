package com.example.mapper;

import com.example.pojo.User;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.env.Environment;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class UserMapper {

    private Environment environment;
    private String PRE_PATH;

    public UserMapper() {
        environment = new StandardEnvironment();
        PRE_PATH = environment.getProperty("application.prePath", String.class, "/home/jeannory-e580/Projects/avro-demo/data/");
    }

    private static ObjectMapper mapper = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    public User jsonToUser(final String file) throws IOException {
        return mapper.readValue(new File(PRE_PATH + "in/" + file),User.class);
    }

    public User[] jsonToUsers(final String file) throws IOException {
        return mapper.readValue(new File(PRE_PATH + "in/"  + file),User[].class);
    }

    public void writeJsonFromAvro(final String file, final User[] users) throws IOException {
        mapper.writeValue(new File(PRE_PATH + "out/" + file), users);
    }

}
