package com.example;

import com.example.mapper.UserMapper;
import com.example.pojo.User;
import com.example.service.AvroServiceImpl;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.FileReader;
import org.apache.avro.file.SeekableInput;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.mapred.FsInput;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.*;

@SpringBootApplication
public class AvroDemoApplication implements CommandLineRunner {

	private static String file = null;
	private static User[] users = null;
	private String PRE_PATH;

	@Autowired
	private AvroServiceImpl avroService;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private Environment environment;

	public static void main(String[] args) throws IOException, InterruptedException {
		SpringApplication.run(AvroDemoApplication.class, args);
	}

	private void resourcesListener(final String pathFolder) throws IOException, InterruptedException {
		final WatchService watchService
				= FileSystems.getDefault().newWatchService();
		final java.nio.file.Path path = new File(pathFolder).toPath();
		path.register(
				watchService,
				StandardWatchEventKinds.ENTRY_CREATE,
				StandardWatchEventKinds.ENTRY_DELETE,
				StandardWatchEventKinds.ENTRY_MODIFY);
		WatchKey key;
		while ((key = watchService.take()) != null) {
			for (WatchEvent<?> event : key.pollEvents()) {
				System.out.println("Event kind:" + event.kind() + ". File affected: "
						+ event.context() + "\n************");
				if (event.kind().name().equals("ENTRY_CREATE")) {

					file = event.context().toString();
					System.out.println("file is : " + file + "\n************");

					System.out.println("serialize to users\n************");
					users = userMapper.jsonToUsers(file);

					file = file.replace(".json", ".avro");
					System.out.println("serialize users to avro - file : " + file + "\n************");
					/**
					 * with schema
					 */
//					AvroServiceImpl.serializeUsersToAvro(file, users, "UserSchema.json");
					/**
					 * without schema
					 */
					avroService.serializeObjectsToAvro(users, file);
					System.out.println("deserialize avro to users\n************");

					users = avroService.deserializeAvroToUsers(file);
					file = file.replace(".avro", "-v2.json");

					System.out.println("write users (avro) to json - file : " + file + "\n************");
					userMapper.writeJsonFromAvro(file, users);
					file = null;
					users= null;
				}
			}
			key.reset();
		}
	}

	@Override
	public void run(String... args) throws Exception {
		PRE_PATH = environment.getProperty("application.prePath", String.class, "/home/jeannory-e580/Projects/avro-demo/data/");
		System.out.println("**********************************************");
		System.out.println("******** Application start *******************");
		System.out.println("**********************************************");
		System.out.println("******** PRE_PATH : " + PRE_PATH);
		System.out.println("**********************************************");
		writeToHadoop();
		readAvroFromHdfs();
		resourcesListener(PRE_PATH + "in/");
	}

	private void writeToHadoop() throws IOException {
		Configuration configuration = new Configuration();
		org.apache.hadoop.fs.FileSystem hdfsFileSystem = FileSystem.get(URI.create("hdfs://0.0.0.0:9000"),configuration);
			hdfsFileSystem.copyFromLocalFile(
					new org.apache.hadoop.fs.Path(PRE_PATH + "out/users1.avro"),
					new org.apache.hadoop.fs.Path("hdfs://0.0.0.0:9000/data/users1.avro"));
	}

	private void readAvroFromHdfs() throws IOException {
		org.apache.hadoop.fs.Path path = new org.apache.hadoop.fs.Path("hdfs://0.0.0.0:9000/data/users1.avro");
		Configuration config = new Configuration(); // make this your Hadoop env config
		SeekableInput input = new FsInput(path, config);
		org.apache.avro.io.DatumReader<org.apache.avro.generic.GenericRecord> reader = new GenericDatumReader<GenericRecord>();
		FileReader<GenericRecord> fileReader = DataFileReader.openReader(input, reader);
		for (GenericRecord datum : fileReader) {
			System.out.println("value = " + datum);
		}
		fileReader.close(); // also closes underlying FsInput
	}

}
