package com.example.service;

import com.example.pojo.CustomSchema;
import com.example.pojo.Field;
import com.example.pojo.User;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.reflect.ReflectData;
import org.apache.avro.reflect.ReflectDatumWriter;
import org.modelmapper.ModelMapper;
import org.springframework.core.env.Environment;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AvroServiceImpl {

    private Environment environment;
    private String PRE_PATH;

    public AvroServiceImpl() {
        environment = new StandardEnvironment();
        PRE_PATH = environment.getProperty("application.prePath", String.class, "/home/jeannory-e580/Projects/avro-demo/data/");
    }

    //    public static void serializeObjectsToAvro(final Object[] objects, final String avroName) throws IOException {
//
//        final File file = new File(PATH_OUT + avroName);
//        final DatumWriter<Object> writer = new ReflectDatumWriter<Object>(Object.class);
//        final DataFileWriter<Object> dataFileWriter = new DataFileWriter(writer);
//        final Schema schema = ReflectData.get().getSchema(Object.class);
//        dataFileWriter.create(schema, file);
//        int i=0;
//        while(i<objects.length){
//            dataFileWriter.append(objects[i]);
//            i+=1;
//        }
//        dataFileWriter.close();
//    }

    public void serializeObjectsToAvro(final User[] users, final String avroName) throws IOException {
        final File file = new File(PRE_PATH + "out/" + avroName);
        final DatumWriter<User> writer = new ReflectDatumWriter<User>(User.class);
        final DataFileWriter<User> dataFileWriter = new DataFileWriter(writer);
        final Schema schema = ReflectData.get().getSchema(User.class);
        dataFileWriter.create(schema, file);
        int i=0;
        while(i<users.length){
            dataFileWriter.append(users[i]);
            i+=1;
        }
        dataFileWriter.close();
    }

    public void serializeObjectsToAvroV2(final com.example.pojo.v2.User[] users, final String avroName) throws IOException {

        final File file = new File(PRE_PATH + "out/" + avroName);
        final DatumWriter<com.example.pojo.v2.User> writer = new ReflectDatumWriter<com.example.pojo.v2.User>(com.example.pojo.v2.User.class);
        final DataFileWriter<com.example.pojo.v2.User> dataFileWriter = new DataFileWriter(writer);
        final Schema schema = ReflectData.get().getSchema(com.example.pojo.v2.User.class);
        dataFileWriter.create(schema, file);
        int i=0;
        while(i<users.length){
            dataFileWriter.append(users[i]);
            i+=1;
        }
        dataFileWriter.close();
    }

    /**
     * @param objects
     * @param avroName
     * @param schemaName
     * @throws IOException
     * @write /data/out/avroName.avro from users with schemaName
     */
//    public static void serializeObjectsToAvro(final Object[] objects, final String avroName, final String schemaName) throws IOException {
//        final File file = new File(PATH_OUT + avroName);
//        final DatumWriter<Object> writer = new ReflectDatumWriter<Object>(Object.class);
//        final DataFileWriter<Object> dataFileWriter = new DataFileWriter(writer);
//        final Schema schema = getUserSchema(schemaName);
//        dataFileWriter.create(schema, file);
//        int i=0;
//        while(i<objects.length){
//            dataFileWriter.append(objects[i]);
//            i+=1;
//        }
//        dataFileWriter.close();
//    }

    public void serializeObjectsToAvro(final User[] users, final String avroName, final String schemaName) throws IOException {
        final File file = new File(PRE_PATH + "out/" + avroName);
        final DatumWriter<User> writer = new ReflectDatumWriter<User>(User.class);
        final DataFileWriter<User> dataFileWriter = new DataFileWriter(writer);
        final Schema schema = getUserSchema(schemaName);
        dataFileWriter.create(schema, file);
        int i=0;
        while(i<users.length){
            dataFileWriter.append(users[i]);
            i+=1;
        }
        dataFileWriter.close();
    }

    public User[] deserializeAvroToUsers(final String file) throws IOException {
        //DeSerializing the objects
        final GenericDatumReader datum = new GenericDatumReader();

        //Instantiating DataFileReader
        final DataFileReader dataFileReader = new DataFileReader(new File(PRE_PATH + "out/" + file), datum);
        final GenericData.Record record = new GenericData.Record(dataFileReader.getSchema());
        List<User> users = new ArrayList<User>();
        while (dataFileReader.hasNext()) {
            dataFileReader.next(record);
            users.add(new User(Integer.parseInt(record.get("id").toString()), record.get("name").toString()));
        }
        dataFileReader.close();
        User[] usersArray = new User[users.size()];
        usersArray = users.toArray(usersArray);
        return usersArray;
    }

    public User[] deserializeAvroToUsers(final String file, final String schemaName) throws IOException {
        //DeSerializing the objects
        final GenericDatumReader datum = new GenericDatumReader();

        //Instantiating DataFileReader
        final DataFileReader dataFileReader = new DataFileReader(new File(PRE_PATH + "out/" + file), datum);

        final GenericData.Record record = new GenericData.Record(getUserSchema(schemaName));

        List<User> users = new ArrayList<User>();
        while (dataFileReader.hasNext()) {
            GenericData.Record record1 = (GenericData.Record) dataFileReader.next(record);
            users.add(new User(Integer.parseInt(record1.get("id").toString()), record1.get("name").toString()));
        }
        dataFileReader.close();
        User[] usersArray = new User[users.size()];
        usersArray = users.toArray(usersArray);
        return usersArray;
    }

    public com.example.pojo.v2.User[] deserializeAvroToUsersV2(final String file, final String schemaName) throws IOException {
        //DeSerializing the objects
        final GenericDatumReader datum = new GenericDatumReader();

        //Instantiating DataFileReader
        final DataFileReader dataFileReader = new DataFileReader(new File(PRE_PATH + "out/" + file), datum);

        final GenericData.Record record = new GenericData.Record(getUserSchema(schemaName));

        List<com.example.pojo.v2.User> users = new ArrayList<com.example.pojo.v2.User>();
        while (dataFileReader.hasNext()) {
            GenericData.Record record1 = (GenericData.Record) dataFileReader.next(record);
            users.add(
                    new com.example.pojo.v2.User(
                            Integer.parseInt(record1.get("id").toString()),
                            record1.get("name").toString(),
                            Integer.parseInt(record1.get("age").toString()))
            );
        }
        dataFileReader.close();
        com.example.pojo.v2.User[] usersArray = new com.example.pojo.v2.User[users.size()];
        usersArray = users.toArray(usersArray);
        return usersArray;
    }

    public Schema deserializeSchema(final String schemaName) throws IOException {
        final File schemaFile = new File(PRE_PATH + "schema/" + schemaName);
        final Schema schema = Schema.parse(schemaFile);
        return schema;
    }

    /**
     * from Schema
     * serialize to json file
     */
    public void writeJsonSchema(final Schema schema, final String schemaName) throws IOException {
        ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        final List<Schema.Field> fields = schema.getFields();
        final Field [] fields1 = new Field[fields.size()];
        int i=0;
        while(i<fields.size()){
            final Field field = new Field();
            fields.toString();
            field.setName(fields.get(i).name());
            final String type = fields.get(i).schema().toString().replace("\"", "");
            field.setType(type);
            fields1[i] = field;
            i+=1;
        }

        final ModelMapper modelMapper = new ModelMapper();
        final CustomSchema customSchema = modelMapper.map(schema, CustomSchema.class);
        customSchema.setType(customSchema.getType().toLowerCase());
        customSchema.setFields(fields1);
        mapper.writeValue(new File(PRE_PATH + "schema/" + schemaName), customSchema);
    }

    private Schema getUserSchema(final String schemaName) throws IOException {
        final File schemaFile = new File(PRE_PATH + "schema/" + schemaName);
         return Schema.parse(schemaFile);
    }

}
